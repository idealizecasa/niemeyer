const gulp = require('gulp');

gulp.task('vendors', () => {
  const files = [
    './node_modules/materialize-css/dist/css/materialize.css',
    './node_modules/materialize-css/dist/js/materialize.js',
    './node_modules/jquery/dist/jquery.js',
    './node_modules/jquery-mask-plugin/dist/jquery.mask.js',
  ];

  files.map((file) => {
    const extension = file.split('.').pop();

    let destination = null;

    if (extension === 'css' || extension === 'scss') {
      destination = './app/assets/stylesheets/vendors';
    } else {
      destination = './app/assets/javascripts/vendors';
    }

    gulp
      .src(file)
      .pipe(gulp.dest(destination));
  });
});

gulp.task('default', ['vendor']);
