# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_20_180341) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "residence_features", force: :cascade do |t|
    t.string "value", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "residence_features_residences", id: false, force: :cascade do |t|
    t.bigint "residence_id", null: false
    t.bigint "residence_feature_id", null: false
    t.index ["residence_feature_id"], name: "index_residence_features_residences_on_residence_feature_id"
    t.index ["residence_id"], name: "index_residence_features_residences_on_residence_id"
  end

  create_table "residence_statuses", force: :cascade do |t|
    t.string "value"
  end

  create_table "residences", force: :cascade do |t|
    t.string "condo_name"
    t.string "address"
    t.string "number"
    t.string "complement"
    t.string "neighborhood"
    t.string "city"
    t.string "state"
    t.string "postal_code"
    t.integer "bedroom"
    t.integer "suite"
    t.integer "bathroom"
    t.integer "living_room"
    t.integer "covered_parking_lot"
    t.integer "uncovered_parking_lot"
    t.integer "total_parking_lot"
    t.boolean "payed_property"
    t.integer "building_aera"
    t.integer "total_area"
    t.boolean "accept_exchange"
    t.string "exchange"
    t.boolean "payed_condo"
    t.string "realstate_county_subscription"
    t.string "residence_registration_number"
    t.float "market_price"
    t.float "sale_price"
    t.float "condo_price"
    t.float "iptu_price"
    t.float "rent_price"
    t.integer "which_floor"
    t.string "tower"
    t.text "description"
    t.string "tour_virtual_link"
    t.string "keys_local"
    t.string "marketing_authorization"
    t.date "suspention_date"
    t.boolean "idealize_commercialization"
    t.float "media_fee"
    t.float "comission_fee"
    t.date "construction_date"
    t.date "reform_date"
    t.integer "aquiring"
    t.integer "acquired"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "residence_status_id"
    t.integer "finality"
    t.integer "capture_type"
    t.index ["residence_status_id"], name: "index_residences_on_residence_status_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
