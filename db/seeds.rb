# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# 

if ResidenceStatus.count < 1
  ResidenceStatus.create(value: "Indicado")
  ResidenceStatus.create(value: "Cadastrando")
  ResidenceStatus.create(value: "Aguardando Autorização")
  ResidenceStatus.create(value: "Suspenso")
  ResidenceStatus.create(value: "Fotos")
  ResidenceStatus.create(value: "Ativo")
  ResidenceStatus.create(value: "Vendido")
end

if ResidenceFeature.count < 1
  ResidenceFeature.create(value: "Apartamento Cobertura")
  ResidenceFeature.create(value: "Armários Embutidos no Quarto")
  ResidenceFeature.create(value: "Armários na Cozinha")
  ResidenceFeature.create(value: "Armários nos Banheiros")
  ResidenceFeature.create(value: "Ar Condicionado")
  ResidenceFeature.create(value: "Banheira de Hidromassagem")
  ResidenceFeature.create(value: "Banheiro de Serviço")
  ResidenceFeature.create(value: "Chuveiro a Gás")
  ResidenceFeature.create(value: "Churrasqueira")
  ResidenceFeature.create(value: "Piscina Privativa")
  ResidenceFeature.create(value: "Próximo a Transporte Público")
  ResidenceFeature.create(value: "Quarto de Serviço")
  ResidenceFeature.create(value: "Quarto Extra Reversível")
  ResidenceFeature.create(value: "Quintal")
  ResidenceFeature.create(value: "Varanda Gourmet")
end
