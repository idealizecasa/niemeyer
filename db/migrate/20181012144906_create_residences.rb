class CreateResidences < ActiveRecord::Migration[5.2]
  def change
    create_table :residences do |t|
      t.string :condo_name
      t.string :address
      t.string :number
      t.string :complement
      t.string :neighborhood
      t.string :city
      t.string :state
      t.string :postal_code
      t.integer :bedroom
      t.integer :suite
      t.integer :bathroom
      t.integer :living_room
      t.integer :covered_parking_lot
      t.integer :uncovered_parking_lot
      t.integer :total_parking_lot
      t.boolean :payed_property
      t.integer :building_aera
      t.integer :total_area
      t.boolean :accept_exchange
      t.string :exchange
      t.boolean :payed_condo
      t.string :realstate_county_subscription
      t.string :residence_registration_number
      t.float :market_price
      t.float :sale_price
      t.float :condo_price
      t.float :iptu_price
      t.float :rent_price
      t.integer :which_floor
      t.string :tower
      t.text :description
      t.string :tour_virtual_link
      t.string :keys_local
      t.string :marketing_authorization
      t.date :suspention_date
      t.boolean :idealize_commercialization
      t.float :media_fee
      t.float :comission_fee
      t.date :construction_date
      t.date :reform_date
      t.integer :aquiring
      t.integer :acquired

      t.timestamps
    end
  end
end
