class CrateJoinTableResidencesResidenceFeatures < ActiveRecord::Migration[5.2]
  def change
    create_join_table :residences, :residence_features do |t|
      t.index :residence_id
      t.index :residence_feature_id
    end
  end
end
