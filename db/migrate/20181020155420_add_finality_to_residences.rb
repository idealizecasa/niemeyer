class AddFinalityToResidences < ActiveRecord::Migration[5.2]
  def change
    change_table :residences do |t|
      t.integer :finality
    end
  end
end
