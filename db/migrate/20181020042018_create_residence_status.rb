class CreateResidenceStatus < ActiveRecord::Migration[5.2]
  def change
    create_table :residence_statuses do |t|
      t.string :value
    end
  end
end
