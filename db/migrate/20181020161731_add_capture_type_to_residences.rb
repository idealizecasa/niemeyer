class AddCaptureTypeToResidences < ActiveRecord::Migration[5.2]
  def change
    change_table :residences do |t|
      t.integer :capture_type
    end
  end
end
