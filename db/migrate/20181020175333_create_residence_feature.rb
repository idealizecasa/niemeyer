class CreateResidenceFeature < ActiveRecord::Migration[5.2]
  def change
    create_table :residence_features do |t|
      t.string :value, null: false
      
      t.timestamps
    end
  end
end
