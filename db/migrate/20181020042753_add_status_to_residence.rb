class AddStatusToResidence < ActiveRecord::Migration[5.2]
  def change
    change_table :residences do |t|
      t.references :residence_status
    end
  end
end
