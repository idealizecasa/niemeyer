require "application_system_test_case"

class ResidencesTest < ApplicationSystemTestCase
  setup do
    @residence = residences(:one)
  end

  test "visiting the index" do
    visit residences_url
    assert_selector "h1", text: "Residences"
  end

  test "creating a Residence" do
    visit residences_url
    click_on "New Residence"

    fill_in "Accept Exchange", with: @residence.accept_exchange
    fill_in "Address", with: @residence.address
    fill_in "Bathroom", with: @residence.bathroom
    fill_in "Bedroom", with: @residence.bedroom
    fill_in "Building Aera", with: @residence.building_aera
    fill_in "City", with: @residence.city
    fill_in "Complement", with: @residence.complement
    fill_in "Condo Name", with: @residence.condo_name
    fill_in "Condo Price", with: @residence.condo_price
    fill_in "Construction Date", with: @residence.construction_date
    fill_in "Covered Parking", with: @residence.covered_parking
    fill_in "Description", with: @residence.description
    fill_in "Estate", with: @residence.estate
    fill_in "Exchange", with: @residence.exchange
    fill_in "How Many Times Acquired", with: @residence.how_many_times_acquired
    fill_in "How Many Times Can Be Acquired", with: @residence.how_many_times_can_be_acquired
    fill_in "Idealize Commercialization", with: @residence.idealize_commercialization
    fill_in "Indicated", with: @residence.indicated
    fill_in "Iptu Price", with: @residence.iptu_price
    fill_in "Living Room", with: @residence.living_room
    fill_in "Market Price", with: @residence.market_price
    fill_in "Marketing Authorization", with: @residence.marketing_authorization
    fill_in "Media Fee", with: @residence.media_fee
    fill_in "Neighborhood", with: @residence.neighborhood
    fill_in "Not Covered Parking", with: @residence.not_covered_parking
    fill_in "Number", with: @residence.number
    fill_in "Payed Condo", with: @residence.payed_condo
    fill_in "Payed Property", with: @residence.payed_property
    fill_in "Postal Code", with: @residence.postal_code
    fill_in "Realstate County Subscription", with: @residence.realstate_county_subscription
    fill_in "Reform Date", with: @residence.reform_date
    fill_in "Rent Price", with: @residence.rent_price
    fill_in "Residence Registration Number", with: @residence.residence_registration_number
    fill_in "Sale Price", with: @residence.sale_price
    fill_in "String", with: @residence.string
    fill_in "Suite", with: @residence.suite
    fill_in "Suspention Date", with: @residence.suspention_date
    fill_in "Total Area", with: @residence.total_area
    fill_in "Total Parking", with: @residence.total_parking
    fill_in "Tour Virtual Link", with: @residence.tour_virtual_link
    fill_in "Tower", with: @residence.tower
    fill_in "Which Floor", with: @residence.which_floor
    click_on "Create Residence"

    assert_text "Residence was successfully created"
    click_on "Back"
  end

  test "updating a Residence" do
    visit residences_url
    click_on "Edit", match: :first

    fill_in "Accept Exchange", with: @residence.accept_exchange
    fill_in "Address", with: @residence.address
    fill_in "Bathroom", with: @residence.bathroom
    fill_in "Bedroom", with: @residence.bedroom
    fill_in "Building Aera", with: @residence.building_aera
    fill_in "City", with: @residence.city
    fill_in "Complement", with: @residence.complement
    fill_in "Condo Name", with: @residence.condo_name
    fill_in "Condo Price", with: @residence.condo_price
    fill_in "Construction Date", with: @residence.construction_date
    fill_in "Covered Parking", with: @residence.covered_parking
    fill_in "Description", with: @residence.description
    fill_in "Estate", with: @residence.estate
    fill_in "Exchange", with: @residence.exchange
    fill_in "How Many Times Acquired", with: @residence.how_many_times_acquired
    fill_in "How Many Times Can Be Acquired", with: @residence.how_many_times_can_be_acquired
    fill_in "Idealize Commercialization", with: @residence.idealize_commercialization
    fill_in "Indicated", with: @residence.indicated
    fill_in "Iptu Price", with: @residence.iptu_price
    fill_in "Living Room", with: @residence.living_room
    fill_in "Market Price", with: @residence.market_price
    fill_in "Marketing Authorization", with: @residence.marketing_authorization
    fill_in "Media Fee", with: @residence.media_fee
    fill_in "Neighborhood", with: @residence.neighborhood
    fill_in "Not Covered Parking", with: @residence.not_covered_parking
    fill_in "Number", with: @residence.number
    fill_in "Payed Condo", with: @residence.payed_condo
    fill_in "Payed Property", with: @residence.payed_property
    fill_in "Postal Code", with: @residence.postal_code
    fill_in "Realstate County Subscription", with: @residence.realstate_county_subscription
    fill_in "Reform Date", with: @residence.reform_date
    fill_in "Rent Price", with: @residence.rent_price
    fill_in "Residence Registration Number", with: @residence.residence_registration_number
    fill_in "Sale Price", with: @residence.sale_price
    fill_in "String", with: @residence.string
    fill_in "Suite", with: @residence.suite
    fill_in "Suspention Date", with: @residence.suspention_date
    fill_in "Total Area", with: @residence.total_area
    fill_in "Total Parking", with: @residence.total_parking
    fill_in "Tour Virtual Link", with: @residence.tour_virtual_link
    fill_in "Tower", with: @residence.tower
    fill_in "Which Floor", with: @residence.which_floor
    click_on "Update Residence"

    assert_text "Residence was successfully updated"
    click_on "Back"
  end

  test "destroying a Residence" do
    visit residences_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Residence was successfully destroyed"
  end
end
