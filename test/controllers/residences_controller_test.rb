require 'test_helper'

class ResidencesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @residence = residences(:one)
  end

  test "should get index" do
    get residences_url
    assert_response :success
  end

  test "should get new" do
    get new_residence_url
    assert_response :success
  end

  test "should create residence" do
    assert_difference('Residence.count') do
      post residences_url, params: { residence: { accept_exchange: @residence.accept_exchange, address: @residence.address, bathroom: @residence.bathroom, bedroom: @residence.bedroom, building_aera: @residence.building_aera, city: @residence.city, complement: @residence.complement, condo_name: @residence.condo_name, condo_price: @residence.condo_price, construction_date: @residence.construction_date, covered_parking: @residence.covered_parking, description: @residence.description, estate: @residence.estate, exchange: @residence.exchange, how_many_times_acquired: @residence.how_many_times_acquired, how_many_times_can_be_acquired: @residence.how_many_times_can_be_acquired, idealize_commercialization: @residence.idealize_commercialization, indicated: @residence.indicated, iptu_price: @residence.iptu_price, living_room: @residence.living_room, market_price: @residence.market_price, marketing_authorization: @residence.marketing_authorization, media_fee: @residence.media_fee, neighborhood: @residence.neighborhood, not_covered_parking: @residence.not_covered_parking, number: @residence.number, payed_condo: @residence.payed_condo, payed_property: @residence.payed_property, postal_code: @residence.postal_code, realstate_county_subscription: @residence.realstate_county_subscription, reform_date: @residence.reform_date, rent_price: @residence.rent_price, residence_registration_number: @residence.residence_registration_number, sale_price: @residence.sale_price, string: @residence.string, suite: @residence.suite, suspention_date: @residence.suspention_date, total_area: @residence.total_area, total_parking: @residence.total_parking, tour_virtual_link: @residence.tour_virtual_link, tower: @residence.tower, which_floor: @residence.which_floor } }
    end

    assert_redirected_to residence_url(Residence.last)
  end

  test "should show residence" do
    get residence_url(@residence)
    assert_response :success
  end

  test "should get edit" do
    get edit_residence_url(@residence)
    assert_response :success
  end

  test "should update residence" do
    patch residence_url(@residence), params: { residence: { accept_exchange: @residence.accept_exchange, address: @residence.address, bathroom: @residence.bathroom, bedroom: @residence.bedroom, building_aera: @residence.building_aera, city: @residence.city, complement: @residence.complement, condo_name: @residence.condo_name, condo_price: @residence.condo_price, construction_date: @residence.construction_date, covered_parking: @residence.covered_parking, description: @residence.description, estate: @residence.estate, exchange: @residence.exchange, how_many_times_acquired: @residence.how_many_times_acquired, how_many_times_can_be_acquired: @residence.how_many_times_can_be_acquired, idealize_commercialization: @residence.idealize_commercialization, indicated: @residence.indicated, iptu_price: @residence.iptu_price, living_room: @residence.living_room, market_price: @residence.market_price, marketing_authorization: @residence.marketing_authorization, media_fee: @residence.media_fee, neighborhood: @residence.neighborhood, not_covered_parking: @residence.not_covered_parking, number: @residence.number, payed_condo: @residence.payed_condo, payed_property: @residence.payed_property, postal_code: @residence.postal_code, realstate_county_subscription: @residence.realstate_county_subscription, reform_date: @residence.reform_date, rent_price: @residence.rent_price, residence_registration_number: @residence.residence_registration_number, sale_price: @residence.sale_price, string: @residence.string, suite: @residence.suite, suspention_date: @residence.suspention_date, total_area: @residence.total_area, total_parking: @residence.total_parking, tour_virtual_link: @residence.tour_virtual_link, tower: @residence.tower, which_floor: @residence.which_floor } }
    assert_redirected_to residence_url(@residence)
  end

  test "should destroy residence" do
    assert_difference('Residence.count', -1) do
      delete residence_url(@residence)
    end

    assert_redirected_to residences_url
  end
end
