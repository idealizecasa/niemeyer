$(document).ready(function() {
  var covered = '#residence_covered_parking_lot';
  var uncovered = '#residence_uncovered_parking_lot';
  var total = '#residence_total_parking_lot';
  
  var sumWith = function(anotherField) {
    var fieldValue = parseInt(this.val()) || 0;
    var anotherFieldValue = parseInt(anotherField.val()) || 0;
  
    $(total).val(fieldValue + anotherFieldValue);
  }
  
  $(covered).on('keyup', sumWith.bind($(covered), $(uncovered)));
  
  $(uncovered).on('keyup', sumWith.bind($(uncovered), $(covered)));
});
