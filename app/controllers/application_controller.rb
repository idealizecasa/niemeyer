class ApplicationController < ActionController::Base
  def redirect_root
    redirect_to admin_residences_url
  end

  protected
  def after_sign_in_path_for(resource)
    admin_residences_path
  end
end
