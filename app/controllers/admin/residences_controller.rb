class Admin::ResidencesController < AdminController
  before_action :set_residence, only: [:show, :edit, :update, :destroy]
  before_action :convert_moneys, only: [:create, :update]
  before_action :convert_dates, only: [:create, :update]

  # GET /residences
  # GET /residences.json
  def index
    @residences = Residence.all
  end

  # GET /residences/1
  # GET /residences/1.json
  def show
  end

  # GET /residences/new
  def new
    @residence = Residence.new
    set_options
  end

  # GET /residences/1/edit
  def edit
    set_options
    disconvert_dates @residence
  end

  # POST /residences
  # POST /residences.json
  def create
    set_options
    @residence = Residence.new(residence_params)

    respond_to do |format|
      if @residence.save
        format.html { redirect_to [:admin, @residence], notice: 'Residence was successfully created.' }
        format.json { render :show, status: :created, location: @residence }
      else
        format.html { render :new }
        format.json { render json: @residence.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /residences/1
  # PATCH/PUT /residences/1.json
  def update
    respond_to do |format|
      if @residence.update(residence_params)
        format.html { redirect_to [:admin, @residence], notice: 'Residence was successfully updated.' }
        format.json { render :show, status: :ok, location: @residence }
      else
        format.html { render :edit }
        format.json { render json: @residence.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /residences/1
  # DELETE /residences/1.json
  def destroy
    @residence.destroy
    respond_to do |format|
      format.html { redirect_to admin_residences_url, notice: 'Residence was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_residence
      @residence = Residence.find(params[:id])
    end

    def convert_moneys
      moneys = %i[
        market_price
        sale_price
        condo_price
        iptu_price
        rent_price
      ]

      moneys.each do |money|
        converted = params[:residence][money] 
        converted = converted.gsub!(".", "")
        converted = converted.gsub!(",", ".")
        converted = converted.to_f
        params[:residence][money] = converted
      end
    end

    def convert_dates
      date_fields.each do |date|
        converted = params[:residence][date] 
        unless converted.blank?
          converted = Date.strptime(converted, '%d/%m/%Y')
          params[:residence][date] = converted
        end
      end
    end

    def disconvert_dates(residence)
      date_fields.each do |date|
        converted = residence[date]
        unless converted.blank?
          converted = converted.strftime("%d/%m/%Y")
        end
        residence[date] = converted
      end
    end

    def date_fields
      %i[
        suspention_date
        construction_date
        reform_date
      ]
    end

    def residence_params
      params.require(:residence).permit(
        :condo_name, :address, :number, :complement, 
        :neighborhood, :city, :state, :postal_code, 
        :bedroom, :suite, :bathroom, :living_room, 
        :covered_parking, :not_covered_parking, 
        :total_parking_lot, :payed_property, :building_aera, 
        :total_area, :accept_exchange, :exchange, :payed_condo, 
        :realstate_county_subscription, :residence_registration_number,
        :market_price, :sale_price, :condo_price,
        :iptu_price, :rent_price, :which_floor, :tower,
        :description, :tour_virtual_link, :marketing_authorization, 
        :indicated, :suspention_date, :idealize_commercialization, 
        :media_fee, :construction_date, :reform_date, 
        :how_many_times_can_be_acquired, :how_many_times_acquired,
        :residence_status_id, :finality, :capture_type,
        residence_feature_ids: []
      )
    end

    def set_options
      @residence_statuses = ResidenceStatus.all
      @finalities = Residence.finalities_options
      @capture_types = Residence.capture_types_options
      @features = ResidenceFeature.all.collect { |cat| [cat.value, cat.id] }
      @selected_features = @residence&.features&.present? ? @residence.features.map(&:id) : []
    end
end
