class Residence < ApplicationRecord
  belongs_to :residence_status
  has_and_belongs_to_many :residence_features

  enum finality: [:sale, :rent, :sale_rent]
  enum capture_type: [:gold, :silver, :promotional]

  def features
    residence_features
  end

  def self.finalities_options
    finalities.map do |finality, _|
      [I18n.t("activerecord.attributes.#{model_name.i18n_key}.finalities.#{finality}"), finality]
    end
  end

  def self.capture_types_options
    capture_types.map do |capture_type, _|
      [I18n.t("activerecord.attributes.#{model_name.i18n_key}.capture_types.#{capture_type}"), capture_type]
    end
  end
end
