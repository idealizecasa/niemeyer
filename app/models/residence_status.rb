class ResidenceStatus < ActiveRecord::Base
  has_many :residences
end
