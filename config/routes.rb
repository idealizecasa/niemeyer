Rails.application.routes.draw do
  devise_for :users

  root to: "application#redirect_root"

  namespace :admin do
    resources :residences
  end
end
